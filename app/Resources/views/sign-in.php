<html>
<head>
    <meta charset="utf-8">
    <link href="/css/style.css" rel="stylesheet">
</head>
<body>

<div id="centered">
    <h2>Войти:</h2>
    <form action="/auth/enter" method="GET">
        <input type="text" name="login" placeholder="Логин">
        <input type="password" name="password" placeholder="Пароль">
        <button type="submit">Войти</button>
    </form>
    <h2><a href="/auth/sign-up">Зарегестрироваться</a></h2>
</div>

</body>
</html>
