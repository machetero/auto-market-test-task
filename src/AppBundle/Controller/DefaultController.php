<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\Compiler\ResolveDefinitionTemplatesPass;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Request as MarketRequest;
use AppBundle\Entity\User;
use AppBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="list")
     */
    public function indexAction(Request $request, Session $session)
    {
        $session->start();
        $token = $session->get('token');
        if (empty($token)){
            return new RedirectResponse('/auth/sign-in');
        }

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['login' => $token]);
        $customer = $this->getDoctrine()->getRepository(User::class)->findOneBy(['login' => $token]);

        if($user->type == 'shop') {
            $requests = $this->getCustomerRequests($token);
        }else{
            $requests = $this->getDoctrine()->getRepository(MarketRequest::class)->findBy(['customer' => $user->login]);
        }

        return $this->render('list.html.twig', ['requests' => $requests, 'user' => $user ]);
    }

    /**
     * @Route("req/req", name="req")
     */
    private function getCustomerRequests($shop)
    {
        $shopProducts = $this->getDoctrine()->getRepository(Product::class)->findByShop($shop);
        $requests = $this->getDoctrine()->getRepository(MarketRequest::class)->findAll();

        $result = [];
        foreach ($shopProducts as $shopProduct) {
            foreach ($requests as $request) {
                if ($request->priceFrom <= $shopProduct->price && $request->priceTo >= $shopProduct->price
                && $request->manufacturer == $shopProduct->manufacturer && $request->product == $shopProduct->name){
                    $result[] = $request;
                    break;
                }
            }
        }

        return $result;
    }
}
