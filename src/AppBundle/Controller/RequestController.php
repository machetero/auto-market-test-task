<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Request as MarketRequest;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class RequestController extends Controller
{
    /**
     * @Route("request/create", name="request_create")
     */
    public function createAction()
    {
        return $this->render('create-request.php');
    }

    /**
     * @Route("request/add", name="request_add")
     */
    public function addAction(Request $request, Session $session)
    {
        if(!$this->productExists(
            $request->request->get('product'), $request->request->get('manufacturer'), $request->request->get('price-from'),
            $request->request->get('price-to')
        )) {
            return new Response('<h2>Таких продуктов нет в продаже</h2>');
        }

        $session->start();
        $token = $session->get('token');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['login' => $token]);

        $marketRequest = new MarketRequest();
        $marketRequest->product = $request->request->get('product');
        $marketRequest->priceFrom = $request->request->get('price-from');
        $marketRequest->priceTo = $request->request->get('price-to');
        $marketRequest->manufacturer = $request->request->get('manufacturer');
        $marketRequest->customer = $user->login;

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($marketRequest);
        $em->flush();

        return new Response('<h2>Запрос добавлен. <a href="/">Назад</a></h2>>');
    }

    private function productExists($productName, $manufacturer, $priceFrom, $priceTo)
    {
        $where = "p.name = '$productName' AND p.manufacturer = '$manufacturer' AND ( p.price BETWEEN $priceFrom AND $priceTo )";

        $qb = $this->getDoctrine()->getEntityManager()->createQueryBuilder();
        $qb->select('p')
            ->from('AppBundle:Product','p')
            ->where($where);
        $result = $qb->getQuery()->getArrayResult();

        return count($result) > 0;
    }
}