<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;

class AuthController extends Controller
{
    /**
     * @Route("auth/sign-in",name="auth_sign_in")
     */
    public function signInAction()
    {
        return $this->render('sign-in.php');
    }

    /**
     * Registration needs captcha protection.
     *
     * @Route("auth/registration", name="auth_registration")
     */
    public function registerAction(Request $request, Session $session)
    {
        $session->start();

        $user =  new User();
        $user->setLogin($request->request->get('login'));
        $user->setType($request->request->get('type'));
        $user->setPassword(password_hash($request->request->get('password'),PASSWORD_DEFAULT));

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->flush();

        $session->set('token',$request->request->get('login'));

        return new RedirectResponse('/');
    }

    /**
     * @Route("auth/sign-up",name="auth_sign_up")
     */
    public function signUpAction()
    {
        return $this->render('sign-up.php');
    }

    /**
     * @Route("auth/sign-out",name="auth_sign_out")
     */
    public function signOutAction(Session $session)
    {
        $session->start();
        $session->invalidate();

        return new RedirectResponse('/');
    }

    /**
     * @Route("auth/enter", name="auth_enter")
     */
    public function enterAction(Request $request, Session $session)
    {
        if(!empty($request->query->get('login')) && !empty($request->query->get('password'))) {
            $rep = $this->getDoctrine()->getRepository(User::class);
            $user = $rep->findOneBy(['login' => $request->query->get('login')]);
        }else{
            return new Response('Неправильный логин и/или пароль <a href="/">Назад</a>');
        }

        if(password_verify($request->query->get('password'), $user->password)){
            $session->start();
            $session->set('token', $request->query->get('login'));
            return new RedirectResponse('/');
        }
        return new RedirectResponse('/');
    }
}