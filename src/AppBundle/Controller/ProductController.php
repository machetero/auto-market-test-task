<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class ProductController extends Controller
{
    /**
     * @Route("product/add", name="product_add")
     */
    public function addAction()
    {
        return $this->render('add-product.php');
    }

    /**
     * Like registration action, it needs captcha protection.
     *
     * @Route("product/save", name="product_save")
     */
    public function saveAction(Request $request, Session $session)
    {
        $session->start();
        $shop = $session->get('token');

        $product = new Product();
        $product->name = $request->request->get('productname');
        $product->price = (int)$request->request->get('productprice');
        $product->manufacturer = $request->request->get('manufacturer');
        $product->shop = $shop;

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($product);
        $em->flush();

        return new RedirectResponse('/');
    }
}