<?php


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="requests")
 */
class Request
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    public $customer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $product;

    /**
     * @ORM\Column(type="integer")
     */
    public $priceFrom;

    /**
     * @ORM\Column(type="integer")
     */
    public $priceTo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $manufacturer;
}